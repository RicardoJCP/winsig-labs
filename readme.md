<h1>Winsig Labs</h1>

<h3>Winsig</h3>

<h5>PWA - Progressive Web Apps</h5>

<ul>
  <li><a href="https://officelink.pt/ws/">Online Test Case</a></li>
  <li><a href="winsig/pwa">Source Code</a></li>
</ul>

<h5>Site</h5>

<ul>
  <li><a href="winsig/">Referencias do SAGE a remover</a></li>
</ul>

<h3>Office Link</h3>

<h5>AMP - Accelerated Mobile Pages</h5>

<ul>
  <li><a href="https://officelink.pt/amp/">Online Version</a></li>
  <li><a href="officelink/amp">Source Code</a></li>
</ul>

<h5>PWA - Progressive Web Apps</h5>

<ul>
  <li><a href="https://officelink.pt/pwa/">Online Version</a></li>
  <li><a href="officelink/pwa">Source Code</a></li>
  <li><a href="officelink/pwa/report.pdf">Report (lighthouse 1.6.5beta)</a></li>
</ul>

<h3>Beatriz Rubio</h3>

<h5>APP - Titanium SDK & Appcelerator Cloud Services</h5>

<ul>
  <li><a href="http://staging.winsig.pt/app/beatriz.rubio.apk">android - v0.0.4.pre-alpha</a></li>
  <li><a href="beatriz.rubio/app">Source Code</a></li>
</ul>

<h1>Documentation</h1>

<h3>Web</h3>

<ul>
  <li><a href="https://whatwebcando.today/">What Web Can Do Today</a></li>
</ul>

<h3>Appcelerator</h3>

<ul>
  <li><a href="http://docs.appcelerator.com/platform/latest/#!/guide/Appcelerator_Command-Line_Interface_Reference">Appcelerator Command-Line Interface Reference</a></li>
  <li><a href="http://docs.appcelerator.com/platform/latest/#!/api/Modules.Cloud">ACS - Appcelerator Cloud Services</a></li>
</ul>

<h3>JavaScript</h3>

<ul>
  <li><a href="http://courses.angularclass.com/p/modern-javascript">Modern Javascript</a></li>
</ul>

<h5>TypeScript</h5>

<ul>
  <li><a href="https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html">TypeScript in 5 minutes</a></li>
</ul>

<h3>Angular</h3>

<ul>
  <li><a href="http://courses.angularclass.com/p/angular-2-fundamentals">Angular 2 Fundamentals</a></li>
</ul>

<h5>Angular + Meteor</h5>

<ul>
  <li><a href="https://www.youtube.com/watch?v=U5ozlhfo6-I&list=PLGtfhst7ZoqBkRMbtHuZxFsGU91cBhHDv">YouTube Playlist</a></li>
  <li><a href="https://www.youtube.com/watch?v=DBGyyxgUxIQ">Angular Meteor</a></li>
  <li><a href="https://www.youtube.com/watch?v=KowwLrakxHo">Starting with angular meteor</a></li>
</ul>