<h2>Developer Outbursts and Traumas</h2>
<h3>Tim Lempicki</h3>
Seriously? Fuck JavaScript -- it's so simple at it's core yet it has been made so ridiculously complex by all the libraries, frameworks, runtimes, template systems, cross-compilers, different versions of ES5 -- ES6, etc etc etc. Not to mention each browser has their own quirks due to using different JS engines and then FUCKKKKKKKKKKKKKKKKKKK
<br>
Oh and each job you go to you have to learn a new framework or library or workflow because every place has their idea of what works best.
<br>
FUCK FUCK FUCK FUCK FUCK -- I'M HAVING A FUCKEN BREAKDOWN....
<br>
I understand continuing education, but trying to be a full stack developer makes me want to just find a different job.
<br>
IE7 is the death of me also -- if I have to support that turd of a browser any more I am going to die a stress related death.
<br>
SOMEONE I NEED A HUG :'(
<br>
Does anyone else find it all so overwhelming? I end up sitting in front of my computer for 6 or more hours after work trying to continue to learn more and more about web development and I started coding at 10 -- 20 years ago. Its impossible to keep up with it all.
<br>
Applying for a job they expect you to have experience working with it all -- everything under the sun.
<br>
(To answer your question I don't have a favorite framework at all -- I've only dabbled with Angular and ended up scrapping the whole thing after a few days, because I felt like I could do what I needed to do much faster with JavaScript by itself and maybe using jQuery library to save myself some time without taking a huge performance hit?)﻿
<br>
<h3>quicklymurderme</h3>
Tim Lempicki story of my life.. start with one framework then realize 2-3 other companies need another framework ugh! I think I've just decided to focus on angular 2 and node & make sure my html5/css3 & sass is good and I'm not even a front end guy. A full stack dev like u. Hang in there, u'll get ur moment :)﻿
<br>
<h3>painnutplay9</h3>
I finished my web developement studies not long ago, less than one year and haven't had a Job at this yet. But after finishing my studies, seeing all this bunch of frameworks, libraries, compilers, transpilers and shit, makes me not want to work at this...
I spend hours trying to learn new things, simply perfecting my Javascript , Html and css knowledge and keeping up to date and all shit and when I see I have to learn all these JS frameworks, SASS, LESS, Node packaging, Gulp, Whatever the fuck all this is.
I am a newbie in this world and I can already feel your pain and I think: "How is this JS worls so fucked up? Javascript whould be so simple once learned it well". But no, all these google, twitter, facebook and big companies' guys hade to come in an make it so complex only geenies and very smart guys can work at this shit making a very elitist job in my opinion. Cause no matter how much effort and time I put into it I will never be up to date.﻿
<br>
By the way, I know Java, Php, Javascript html, css, mysql (forgot a little bit, since I don't use it in a regular basis), and i don't really know where I should focus, I learned all this for web developent, which involves front-end and back-end. I see front-end is more overwhealming, but IDK, maybe if Node is considered also backed.
So yeah, I don't know where to focus my daily updating knowledge and constant learning, where to continue, to focus on a language, not learn new ones, learn Javascript frameworks or what.
<h3>Tim Lempicki</h3>
I completely agree. If you are a good programmer you can learn any framework / language syntax easily. It's the programming concepts, patterns, and theories you need to know. Experience with JavaScript obviously helps a lot, but if you know JavaScript theres no reason why you can't just pick up whatever framework or library they want you to use at the time. It's impossible to keep up with them all, and it's really hard to keep up with the popular ones. You aren't going to be getting a lot of useful experience with them all even if you do know them.
<br>
Yet, every job description asks you know every framework of every language as an entry level position. It makes no sense. They clearly have no idea how programming works.
<br>
<h2>References</h2>
<a href="https://www.youtube.com/watch?v=pobnhNY5znQ">YouTube</a>
<br>
<a href="http://stateofjs.com/2016/introduction/">State of JS</a>