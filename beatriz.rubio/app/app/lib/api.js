
var ACS = require('ti.cloud');

exports.init = function() {
  
    ACS.Users.login({
        login: 'appc-app',
        password: 'appc-winsig',
    }, function(result) {

        console.warn(result);

        result = null;
    });
};

exports.getData = function(args) {

    if(args) {

        var classname = args.type || "posts";

        ACS.Objects.query({
            classname: classname,
            page: args.offset || 1,
            per_page: args.limit || 10
        }, function(result) {
            
            args && args.callback && args.callback(result.success ? (result[classname] || false) : false);

            args = result = classname = null;
        });
    }
};

exports.newItem = function(args, fieldset) {

    args && ACS.Objects.create({
        classname: args.type || "posts",
        fields: fieldset
    }, function(result) {
        
        args && args.callback && args.callback(result.success);

        args = fieldset = null;
    });
};