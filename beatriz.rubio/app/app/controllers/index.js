var performCalendarReadFunctions = function() {

	console.log("OK CAL");
};

Ti.Calendar.hasCalendarPermissions() ? performCalendarReadFunctions() : Ti.Calendar.requestCalendarPermissions(function(e) {
	
	if(e.success) performCalendarReadFunctions();
	else {
		Ti.API.error(e.error);
		alert('Access to calendar is not allowed');
	}
});

var selected,
	views = {},
	popup = Alloy.createController("popup"),
	menu = Alloy.createController("menu", {
		items: [{
			alias: "this_month",
			title: "Este Mês"
		}, {
			alias: "bucket_list",
			title: "20 Coisas"
		}, {
			alias: "vouchers",
			title: "Vales"
		}, {
			alias: "calendar",
			title: "Calendário"
		}, {
			alias: "holidays",
			title: "Feriados"
		}, {
			alias: "options",
			title: "Opções"
		}, {
			alias: "about",
			title: "Sobre"
		}, {
			alias: "creator",
			title: "Criar Registos"
		}],
		close: function() {

			$.win.remove(menu.getView());
		},
		click: function(page, title) {

			selected && $.content.remove(views[selected].getView());

			if(!views[page]) views[page] = Alloy.createController("content/" + page);

			$.content.add(views[page].getView());

			$.title.text = title;

			selected = page;

			page = title = null;
		}
	});


$.header.addEventListener("click", function() {

	$.win.add(menu.getView());

	menu.openMenu();
});


showPopup = function(args) {

	args && popup.update && popup.update(args);

	$.win.add(popup.getView());

	args = null;
};


hidePopup = function(cleanup) {

	cleanup && popup.cleanup && popup.cleanup();

	$.win.remove(popup.getView());

	cleanup = null;
};


$.win.open();
