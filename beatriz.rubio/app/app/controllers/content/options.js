
var views = {};


views.langOptions = Alloy.createController("ui/form.options", {
    label: "Idioma",
    options: [{
        title: "Português (PT)",
        alias: "pt"
    }, {
        title: "English (UK)",
        alias: "en"
    }, {
        title: "Francois",
        alias: "fr"
    }, {
        title: "Español",
        alias: "es"
    }]
}).getView();


$.page.add(views.langOptions);


views.syncCalendar = Alloy.createController("ui/form.checkbox", {
    label: "Sincronizar Calendário"
}).getView();


$.page.add(views.syncCalendar);


views.syncEvents = Alloy.createController("ui/form.checkbox", {
    label: "Sincronizar Lembretes"
}).getView();


$.page.add(views.syncEvents);


$.cleanup = function() {

    $.destroy();

    $.off();
    
    views && _.each(views, function(view) {

        view && view.cleanup && view.cleanup();

        view = null;
    });
    
    views = null;
};