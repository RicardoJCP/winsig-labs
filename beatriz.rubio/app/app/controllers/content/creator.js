

var API = require('api');


var type = Alloy.createController("ui/form.options", {
    label: "Tipo",
    options: [{
        title: "Vales",
        value: "Vouchers"
    }, {
        title: "20 Coisas",
        value: "BucketList"
    }]
}).getView();


$.type.add(type);


$.save.addEventListener("click", function() {

    API.newItem({
        type: type.getValue(),
        callback: function(result) {

            console.warn(result);

            result = null;
        }
    }, {
        title: $.title.value,
        desc: $.desc.value
    });
});