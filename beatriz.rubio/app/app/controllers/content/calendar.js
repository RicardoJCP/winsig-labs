
var views = {};

var currentYear = 2017;

var defCalendar = Ti.Calendar.defaultCalendar;
    var date1 = new Date(new Date().getTime() + 3000),
        date2 = new Date(new Date().getTime() + 900000);

 defCalendar.createEvent({
        title: 'Sample Event',
        notes: 'This is a test event which has some values assigned to it.',
        location: 'Appcelerator Inc',
        begin: date1,
        end: date2,
        availability: Ti.Calendar.AVAILABILITY_FREE,
        allDay: false,
});

function printEvent(event) {
          if (event.allDay) {
            console.log('Event: ' + event.title + '; ' + event.begin + ' (all day)');
          } else {
            console.log('Event: ' + event.title + '; ' + event.begin + ' ' + event.begin+ '-' + event.end);
          }

          var reminders = event.reminders;
          if (reminders && reminders.length) {
              console.log('There is/are ' + reminders.length + ' reminder(s)');
              for (var i = 0; i < reminders.length; i++) {
                //  printReminder(reminders[i]);
              }
          }
          console.log('hasAlarm? ' + event.hasAlarm);
          var alerts = event.alerts;
          if (alerts && alerts.length) {
            for (var i = 0; i < alerts.length; i++) {
             // printAlert(alerts[i]);
            }
          }

          var status = event.status;
          if (status == Ti.Calendar.STATUS_TENTATIVE) {
            console.log('This event is tentative');
          }
          if (status == Ti.Calendar.STATUS_CONFIRMED) {
            console.log('This event is confirmed');
          }
          if (status == Ti.Calendar.STATUS_CANCELED) {
            console.log('This event was canceled');
          }
        }

 var events = defCalendar.getEventsInYear(currentYear);
 
        if (events && events.length) {
          console.log(events.length + ' event(s) in ' + currentYear);
          console.log('');
          for (var i = 0; i < events.length; i++) {
            printEvent(events[i]);
            console.log('');
          }
        } else {
          console.log('No events');
        }


$.cleanup = function() {

    $.destroy();

    $.off();
    
    views && _.each(views, function(view) {

        view && view.cleanup && view.cleanup();

        view = null;
    });
    
    views = null;
};