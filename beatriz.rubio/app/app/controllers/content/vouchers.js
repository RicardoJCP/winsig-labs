
var views = {};


var API = require('api');

API.getData({
    type: 'Vouchers',
    callback: function(result) {

        var items = [];
            
        result.forEach(function(item) {

            items.push({
                title: {
                    text: item.title
                },
                desc: {
                    text: item.desc
                }
            });

            item = null;
        });

        $.list.sections[0].setItems(items);

        result = items = null;
    }
});

$.cleanup = function() {

    $.destroy();

    $.off();
    
    views && _.each(views, function(view) {

        view && view.cleanup && view.cleanup();

        view = null;
    });
    
    views = null;
};