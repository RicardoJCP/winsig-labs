
var value = false;


$.label.text = $.args.label;


var updateCheckbox = function() {

    if($.icon) {

        $.icon.borderColor = value ? "black" : "gray";

        $.icon.backgroundColor = value ? "green" : "white";
    }
};


updateCheckbox();


$.element.addEventListener("click", function() {

    value = !value;

    updateCheckbox();
});


$.element.cleanup = function() {

    $.destroy();

    $.off();

    value = null;
};