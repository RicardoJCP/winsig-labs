
$.alias = $.element.alias = $.args.alias;

$.element.title = $.title.text = $.args.title;


$.element.selected = function(status) {

    $.title.color = status ? "red" : "#888";

    status = null;
};


$.element.cleanup = function() {

    $.destroy();

    $.off();
};