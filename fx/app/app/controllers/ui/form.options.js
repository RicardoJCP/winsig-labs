
var field = {
    title: '',
    value: '',
    alias: ''
};


$.label.text = $.args.label;


var picker = Ti.UI.createPicker();

picker.addEventListener("change", function(result) {

    if(result && result.row) {
    
        ["title", "value", "alias"].forEach(function(property) {

            field[property] = result.row[property];

            property = null;
        });

        if(!field.value && field.title) field.value = field.title;

        $.value.text = field.title;
    }

    result = null;
});

picker.add($.args.options);


$.element.addEventListener("click", function() {

    $ && $.args && $.args.options && showPopup({
        title: $.args.label || "Selecionar",
        content: picker
    });
});


$.element.setValue = function(value) {

    if(value) field = {
        title: value,
        value: value,
        alias: value
    };

    $.value.text = field.title;

    value = null;
};


$.element.getValue = function() {

    return field.value;
};


$.element.setSelected = function(properties) {

    if(properties) field = properties;

    $.value.text = field.title;

    properties = null;
};


$.element.getSelected = function() {

    return field;
};


$.element.cleanup = function() {

    $.destroy();

    $.off();

    picker = null;

    field = null;
};