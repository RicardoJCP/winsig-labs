

var enabled,
    selected,
    views = {},
    width = {
        menu: Ti.Platform.displayCaps.platformWidth
    };


width.padding = parseInt(width.menu * .20);

width.content = parseInt(width.menu - width.padding);

$.menu.width = width.menu;

$.content.width = width.content;

$.content.left = -width.content;


$.args.items.forEach(function(item) {

    if(item && item.alias) {

        views[item.alias] = Alloy.createController("ui/menu.button", item).getView();

        $.nav.add(views[item.alias]);
    }

    item = null;
});

$.menu.addEventListener("click", function(item) {

    if(enabled && item && item.source && item.source.alias && $.args) {
        
        if(item.source.alias != "close") {

            selected && views[selected] && views[selected].selected && views[selected].selected(false);

            selected = item.source.alias;
            
            $.args.click && $.args.click(selected, item.source.title || "");

            views[selected] && views[selected].selected && views[selected].selected(true);
        }

        $.closeMenu();
    }

	item = null;
});

$.openMenu = function(callback) {

    enabled = false;

    $.menu.animate({
        backgroundColor: "#A6000000",
        duration: 150
    });

    $.content.animate({
        left: 0,
        duration: 250
    }, function() {

        callback && callback();

        enabled = true;

        callback = null;
    });
};

$.closeMenu = function(callback) {

    enabled = false;

    $.menu.animate({
        backgroundColor: "#0D000000",
        duration: 150
    });

    $.content.animate({
        left: -width.content,
        duration: 250
    }, function() {

        $.args && $.args.close && $.args.close();

        callback && callback();

        enabled = true;

        callback = null;
    });
};