
$.update = function(args) {
    
    if(args) {

        $.title.text = args.title || "";

        args.content && $.content.add(args.content);
    }

    args = null;
};

$.cleanup = function() {
    
    $.title.text = "";
    
    $.content.children.forEach(function(view) {

        view && view.cleanup && view.cleanup();

        view = null;
    });

    $.content.removeAllChildren();
};

$.cancel.addEventListener("click", function() {

    hidePopup();
});

$.select.addEventListener("click", function() {

    hidePopup();
});