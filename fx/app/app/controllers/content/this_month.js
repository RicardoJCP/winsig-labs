
var views = {};


$.cleanup = function() {

    $.destroy();

    $.off();
    
    views && _.each(views, function(view) {

        view && view.cleanup && view.cleanup();

        view = null;
    });
    
    views = null;
};