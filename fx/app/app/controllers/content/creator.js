

var API = require('api');

var form = require('form');

var views = {};

views.form = Alloy.createController("ui/form", {
    groups: [{
        alias: "company",
        fieldset: [{
            alias: "client",
            title: "Clientes",
            fields: [{
                alias: "no",
                name: "Nº"
            }, {
                alias: "name",
                name: "Nome"
            }]
        }, {
            alias: "equipment",
            title: "Equipamento",
            fields: [{
                alias: "serie",
                name: "Nº de Serie"
            }, {
                alias: "marca",
                name: "Marca"
            }, {
                alias: "modelo",
                name: "Modelo"
            }]
        }]
    }, {
        alias: "service",
        fieldset: [{
            alias: "pat",
            title: "PAT",
            fields: [{
                alias: "nopat",
                name: "ID"
            }, {
                alias: "realizado",
                name: "Realizado"
            }]
        }, {
            alias: "intervention",
            title: "Intervention",
            fields: [{
                alias: "mhid",
                name: "ID"
            }, {
                alias: "mhtipo",
                name: "Tipo"
            }, {
                alias: "u_princ",
                name: "Principal"
            }, {
                alias: "u_fs",
                name: "Folha de Serviço"
            }]
        }]
    }, {
        alias: "info",
        fieldset: [{
            alias: "times",
            title: "Tempos",
            fields: [{
                alias: "hora",
                name: "Hora de Início"
            }, {
                alias: "horaf",
                name: "Hora de Fim"
            }, {
                alias: "data",
                name: "Data"
            }]
        }]
    }]
});


$.type.add(views.form.getView());


var type = Alloy.createController("ui/form.options", {
    label: "Tipo",
    options: [{
        title: "Vales",
        value: "Vouchers"
    }, {
        title: "20 Coisas",
        value: "BucketList"
    }]
}).getView();


$.type.add(type);


$.save.addEventListener("click", function() {

    API.newItem({
        type: type.getValue(),
        callback: function(result) {

            console.warn(result);

            result = null;
        }
    }, {
        title: $.title.value,
        desc: $.desc.value
    });
});