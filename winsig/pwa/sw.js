self.addEventListener('install', function(event) {

  event.waitUntil(caches.open('v1').then(function(cache) {

    return cache.addAll([
      '/',
      'index.html',

      'assets/images/empty.jpg',
      'assets/images/img.png',
      'assets/images/bg.png',
      'assets/images/logo.png',

      'page/garanta-a-seguranca-do-seu-negocio-e-da-sua-empresa/index.html',
      'page/garanta-a-seguranca-do-seu-negocio-e-da-sua-empresa/img.jpg',

      'page/marca-phc-regista-o-melhor-ano-de-vendas/index.html',
      'page/marca-phc-regista-o-melhor-ano-de-vendas/img.jpg',

      'page/gestao-phc-ja-conhece-a-nova-phc-drive-fx-store/index.html',
      'page/gestao-phc-ja-conhece-a-nova-phc-drive-fx-store/img.jpg'
    ]).then(function() {

			console.log("AVAILABLE OFFLINE");
		});
  }));
});

self.addEventListener('fetch', function(event) {

	event.respondWith(caches.match(event.request).then(function(response) {

		return response || fetch(event.request);
	}));
});
